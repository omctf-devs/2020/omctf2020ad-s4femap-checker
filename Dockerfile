FROM python:3.7

COPY /checker /s4femap/checker
COPY requirements.txt /s4femap/requirements.txt

WORKDIR /s4femap

RUN pip install -r requirements.txt

EXPOSE 8081

ENTRYPOINT ["python"]
CMD ["-u", "checker/service.py"]
