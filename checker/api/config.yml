openapi: "3.0.0"

info:
  version: 1.0.0
  title: Checker API

paths:
  /info:
    get:
      operationId: api.handler.info_get
      summary: Get info about checker
      responses:
        '200':
          description: "Return info about checker. Should be the same for all checker instances. Executed 1 time in the beginning of the game"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Info"
        default:
          description: unexpected error

  /flag:
    post:
      operationId: api.handler.flag_post
      summary: Place flag
      requestBody:
        description: Flag to place
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Flag'
      responses:
        '200':
          description: Flag is placed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/State'
        '404':
          description: Service data is corrupted
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        '500':
          description: Checker internal error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        '502':
          description: Service API is corrupted
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        '503':
          description: Service is down
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string

    put:
      operationId: api.handler.flag_put
      summary: Check flag
      requestBody:
        description: Flag to place
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Flag'
      responses:
        '200':
          description: Flag is placed
        '404':
          description: Service data is corrupted
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        '500':
          description: Checker internal error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        '502':
          description: Service API is corrupted
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        '503':
          description: Service is down
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string

  /hosts/{host}:
    get:
      operationId: api.handler.hosts_get
      summary:  Check general functionality of service
      parameters:
        - name: host
          in: path
          required: true
          description: Host of the flag. Can be FQDN, IPv4 or IPv6 addresses
          schema:
            type: string
      responses:
        '200':
          description: Service works fine
        '500':
          description: Checker internal error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        '502':
          description: Service API is corrupted
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        '503':
          description: Service is down
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
            text/plain:
              schema:
                type: string
            text/html:
              schema:
                type: string

components:
  schemas:
    Info:
      type: object
      properties:
        version:
          type: string
          description: Version of the checker using part of the semantic versioning.
          pattern: '^\d{1,3}\.\d{1,3}(\.\d{1,5})+$'
        sequence:
          type: array
          items:
            type: integer
          description: |
            Most complicated part of checker. Should return placement sequence of vulneravbilites in service.
            Let's assume that we have 4 vulnerabilities. If checker returns [1,1,2,3,3,3,3,4,4,4]
            the check system will set 2 flags for 1st vulnerability, then 1 flag for 2nd vulnerability,
            then 4 flags for 3rd vulnerability and at last 3 flags for 4th vulnerability and around the circle.
      required:
        - sequence

    State:
      type: object
      required:
        - state
      description: Data, which needed for flag check. Is returned during flag placements
      properties:
        state:
          type: string
          format: byte
          description: |
            State associated with flag. Checker can store up to 1024 B of data for specific flag.
            State can be returned by checker during flag placement and will be saved in check system
            It's not prefered way to store state, but it's good enough for extremely simple checkers.

    Flag:
      type: object
      required:
        - host
        - vuln
        - flag
      properties:
        host:
          type: string
          description: Host of the flag. Can be FQDN, IPv4 or IPv6 addresses
        vuln:
          type: integer
          description: Vulnerability number
        flag:
          type: string
          description: Flag (can be any arbitrary string). In most cases it's '^[A-F0-9]{31}=$'
        state:
          type: string
          format: byte
          description: |
            State associated with flag. Checker can store up to 1024 B of data for specific flag.
            State can be returned by checker during flag placement and will be saved in check system
            It's not prefered way to store state, but it's good enough for extremely simple checkers.

    Error:
      type: object
      description: Error message
      required:
        - code
        - message
      properties:
        code:
          type: integer
          format: int32
        message:
          description: Public error message. DO NOT place confidential information here (keys, tokens, methods and so on)
          type: string
        debug:
          description: Place debug info or extended error message here.
          type: string

