import sys
from sys import exc_info
import base64

import requests.utils

from methods.result import (
    Result,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    ERROR,
    DOWN,
)
from s4femap.requests import (
    login,
    get_liked_marks,
    get_mark_comments,
)
from utils import (
    traceback_to_string,
    RestException,
    InconsistentException,
)


def get_flag(host, uid, flag, vuln):
    # check that flag exists in certain vulnerability
    try:
        with requests.Session() as s:
            if vuln == 1:
                return get_first_vuln_flag(s, host, uid, flag)
            elif vuln == 2:
                return get_second_vuln_flag(s, host, uid, flag)
            elif vuln == 3:
                return get_third_vuln_flag(s, host, uid, flag)
            elif vuln == 4:
                return get_fourth_vuln_flag(s, host, uid, flag)
            else:
                print("Invalid vuln", file=sys.stderr)
                return Result(ERROR, ('Invalid vulnerability index: %s' % vuln))
    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API Error', str(re))
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, 'Service is down', str(ce))
    except:
        print('unknown err', exc_info())
        return Result(ERROR, 'Unknown error', traceback_to_string())


def get_credentials(uid):
    uid_bytes = str.encode(uid, 'utf-8')
    cred_bytes = base64.b64decode(uid_bytes)
    cred_str = cred_bytes.decode('utf-8')

    creds = cred_str.split(':')
    if len(creds) < 2:
        raise InconsistentException("Invalid uid")
    return creds[0], creds[1]


def get_first_vuln_flag(session, host, uid, flag):
    username, password = get_credentials(uid)
    token, _ = login(session, host, username, password)
    liked_marks = get_liked_marks(session, host, token)

    mark_with_flag = next((mark for mark in liked_marks if mark.get('name') == flag), None)
    if mark_with_flag:
        return Result(SUCCESS, 'flag is still here')

    return Result(CORRUPT, 'flag is corrupted')


def get_second_vuln_flag(session, host, uid, flag):
    username, password = get_credentials(uid)
    _, response_body = login(session, host, username, password)
    print(response_body)
    if response_body.get('data').get('personalID') == flag:
        return Result(SUCCESS, 'flag is still here')

    return Result(CORRUPT, 'flag is corrupted')


def get_third_vuln_flag(session, host, uid, flag):
    username, password = get_credentials(uid)
    token, _ = login(session, host, username, password)
    liked_marks = get_liked_marks(session, host, token)

    for mark in liked_marks:
        if mark.get('author') != username:
            continue
        comments = get_mark_comments(session, host, token, mark.get('id'))
        for comment in comments.get('data'):
            if comment.get('description') == flag:
                return Result(SUCCESS, 'flag is still here')

    return Result(CORRUPT, 'flag is corrupted')


def get_fourth_vuln_flag(session, host, uid, flag):
    username, password = get_credentials(uid)
    token, _ = login(session, host, username, password)
    liked_marks = get_liked_marks(session, host, token)

    mark_with_flag = next((mark for mark in liked_marks if mark.get('name') == flag), None)
    if mark_with_flag:
        return Result(SUCCESS, 'flag is still here')

    return Result(CORRUPT, 'flag is corrupted')
