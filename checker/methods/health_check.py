import sys
from sys import exc_info

import requests.utils
from faker import Faker

from methods.result import (
    Result,
    SUCCESS,
    MUMBLE,
    DOWN,
    ERROR,
)
from s4femap.requests import (
    registration,
    login,
    create_mark,
    like_mark,
    comment_mark,
    get_liked_marks,
    get_couriers,
    get_bunkers,
)
from utils import (
    rand_str,
    string2numeric_hash,
    traceback_to_string,
    RestException,
    InconsistentException,
)
from settings import (
    DEBUG,
)

fake = Faker()


def health_check(host):
    # check some service endpoints to make sure it works correctly
    try:
        with requests.Session() as s:
            username = fake.user_name()
            password = string2numeric_hash(rand_str())
            user_extras = {
                'personalID': username,
            }
            registration(s, host, username, password, user_extras)
            token, _ = login(s, host, username, password)

            resp = create_mark(
                s, host, token,
                x=fake.random_int(0, 100),
                y=fake.random_int(0, 100),
                name=fake.pystr(min_chars=8, max_chars=16),
                sensor_code=fake.pystr(min_chars=8, max_chars=8),
                mark_type='anomaly',
                is_private=True,
            )
            mark = resp.get('data')

            like_mark(s, host, token, mark.get('id'))

            comment_mark(
                s, host, token,
                mark_id=mark.get('id'),
                comment="Some comment",
                is_private=True,
            )

            liked_marks = get_liked_marks(s, host, token)
            if len(liked_marks) == 1:
                liked_mark = liked_marks[0]
                if not liked_mark.get('comments') == 1:
                    raise InconsistentException(
                        "Get user mark: mark should have only one comment"
                    )
                if not liked_mark.get('likes') == 1:
                    raise InconsistentException(
                        "Get user mark: mark should have only one like"
                    )
                mark.pop('comments')
                mark.pop('likes')
                mark.pop('isLiked')
                if not (mark.items() <= liked_mark.items()):
                    if DEBUG:
                        print(mark)
                        print()
                        print(liked_mark)

                    raise InconsistentException(
                        "Get user mark: got mark is corrupted"
                    )
            else:
                raise InconsistentException(
                    "Test user created during check should have one associated liked mark"
                )

            get_couriers(s, host, token)
            get_bunkers(s, host, token)

            return Result(SUCCESS, "It's alive!")

    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API error', str(re))
    except InconsistentException as inc:
        print(inc, file=sys.stderr)
        return Result(MUMBLE, 'Service API returned inconsistent data', str(inc))
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, 'Service is down', str(ce))
    except:
        return Result(ERROR, 'Unknown error', traceback_to_string())
