from methods.result import (
    Result,
    SUCCESS,
)


def info():
    return Result(SUCCESS, extra_data={
        'sequence': [1, 1, 1, 1],
        'version': '1.0',
    })
