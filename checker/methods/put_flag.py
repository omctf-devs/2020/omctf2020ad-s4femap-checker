import sys
from sys import exc_info
import base64

import requests.utils
from faker import Faker

from s4femap.requests import (
    registration,
    create_mark,
    like_mark,
    comment_mark,
)
from methods.result import (
    Result,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    ERROR,
    DOWN,
)
from utils import (
    rand_str,
    string2numeric_hash,
    traceback_to_string,
    RestException,
)

fake = Faker()


def put_flag(host, flag, vuln):
    # put flags in different vulnerabilities (uid not used, because it return own id, which consists of login:password)
    vuln = int(vuln)
    if not (1 <= vuln <= 4):
        print("Invalid vuln", file=sys.stderr)
        return ERROR

    try:
        with requests.Session() as s:
            if vuln == 1:
                return put_first_vuln_flag(s, host, flag)
            elif vuln == 2:
                return put_second_vuln_flag(s, host, flag)
            elif vuln == 3:
                return put_third_vuln_flag(s, host, flag)
            elif vuln == 4:
                return put_fourth_vuln_flag(s, host,flag)
            else:
                print("Invalid vuln", file=sys.stderr)
                return Result(ERROR, ('Invalid vulnerability index: %s' % vuln))
    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API Error', str(re))
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, 'Service is down', str(ce))
    except:
        print('unknown err', exc_info())
        return Result(ERROR, 'Unknown error', traceback_to_string())


def generate_uid(username, password):
    cred_str = '%s:%s' % (username, password)
    uid_bytes = str.encode(cred_str, 'utf-8')
    uid_base64_bytes = base64.encodebytes(uid_bytes)
    uid = uid_base64_bytes.decode('utf-8')

    return uid


def put_first_vuln_flag(session, host, flag):
    username = fake.user_name().capitalize()    
    password = string2numeric_hash(rand_str())
    user_extras = {
        'personalID': username,
    }
    token, _ = registration(session, host, username, password, user_extras)
    resp = create_mark(
        session, host, token,
        x=fake.random_int(0, 100),
        y=fake.random_int(0, 100),
        name=flag,
        sensor_code=fake.pystr(min_chars=8, max_chars=8),
        mark_type='anomaly',
        is_private=True,
    )
    mark = resp.get('data')

    like_mark(session, host, token, mark.get('id'))

    uid = generate_uid(username, password)
    print('uid: %s' % uid)

    return Result(SUCCESS, extra_data=uid)


def put_second_vuln_flag(session, host, flag):
    username = fake.user_name()
    password = string2numeric_hash(rand_str())
    user_extras = {
        'personalID': flag,
    }
    registration(session, host, username, password, user_extras)

    uid = generate_uid(username, password)
    print('uid: %s' % uid)

    return Result(SUCCESS, extra_data=uid)


def put_third_vuln_flag(session, host, flag):
    username = fake.user_name()
    password = string2numeric_hash(rand_str())
    user_extras = {
        'personalID': username,
    }
    token, _ = registration(session, host, username, password, user_extras)
    resp = create_mark(
        session, host, token,
        x=fake.random_int(0, 100),
        y=fake.random_int(0, 100),
        name=fake.pystr(min_chars=8, max_chars=16),
        sensor_code=fake.pystr(min_chars=8, max_chars=8),
        mark_type='anomaly',
        is_private=False,
    )
    mark = resp.get('data')

    like_mark(session, host, token, mark.get('id'))
    comment_mark(
        session, host, token,
        mark_id=mark.get('id'),
        comment=flag,
        is_private=True,
    )

    uid = generate_uid(username, password)
    print('uid: %s' % uid)

    return Result(SUCCESS, extra_data=uid)


def put_fourth_vuln_flag(session, host, flag):
    username = fake.user_name()
    password = string2numeric_hash(rand_str())
    user_extras = {
        'personalID': username,
    }
    token, _ = registration(session, host, username, password, user_extras)
    resp = create_mark(
        session, host, token,
        x=fake.random_int(0, 100),
        y=fake.random_int(0, 100),
        name=flag,
        sensor_code=fake.pystr(min_chars=8, max_chars=8),
        mark_type='loot',
        is_private=True,
    )
    mark = resp.get('data')

    like_mark(session, host, token, mark.get('id'))

    uid = generate_uid(username, password)
    print('uid: %s' % uid)

    return Result(SUCCESS, extra_data=uid)





