ERROR = 110
SUCCESS = 101
CORRUPT = 102
MUMBLE = 103
DOWN = 104


class Result:
    def __init__(self, status, message='', debug='', extra_data=None):
        self.status = status
        self.message = message
        self.debug = debug
        self.extra_data = extra_data
