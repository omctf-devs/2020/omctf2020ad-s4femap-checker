import json

from settings import (
    TIMEOUT
)
from s4femap.api_urls import (
    get_url,
)
from s4femap.utils import (
    print_request,
    handle_response,
)


def registration(session, hostname, username, password, extras):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ',
    }
    data = json.dumps({
        'login': username,
        'password': password,
        'personalID': extras.get('personalID'),
    })
    url = get_url(hostname, 'user')

    response = session.post(
        url,
        headers=headers,
        data=data,
        timeout=TIMEOUT,
    )

    print_request('POST', response.url, headers, data)
    handle_response(response, 'Registration')

    response_body = response.json()

    return response_body.get('token'), response_body


def login(session, hostname, username, password):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ',
    }
    data = json.dumps({
        'login': username,
        'password': password,
    })
    url = get_url(hostname, 'login')

    response = session.post(
        url,
        headers=headers,
        data=data,
        timeout=TIMEOUT,
    )

    print_request('POST', response.url, headers, data)
    handle_response(response, 'Authorization')

    response_body = response.json()

    return response_body.get('token'), response_body


def get_couriers(session, hostname, token):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'user')

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT,
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get couriers')

    return response.json()


def get_bunkers(session, hostname, token):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'bunkers')

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT,
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get bunkers')

    return response.json()


def create_mark(
    session, hostname, token,
    x=0, y=0,
    name='Some Mark', mark_type='anomaly', sensor_code='sensor_12345',
    is_private=True, shared_with='',
):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
    }
    data = json.dumps({
        'x': x,
        'y': y,
        'name': name,
        'markType': mark_type,
        'sensorCode': sensor_code,
        'isPrivate': is_private,
        'sharedWith': shared_with,
    })
    url = get_url(hostname, 'mark')

    response = session.post(
        url,
        headers=headers,
        data=data,
        timeout=TIMEOUT,
    )

    print_request('POST', response.url, headers, data)
    handle_response(response, 'Create mark')

    return response.json()


def like_mark(session, hostname, token, mark_id):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    base_url = get_url(hostname, 'mark')
    url = "%s/%s/like" % (base_url, mark_id)

    response = session.post(
        url,
        headers=headers,
        timeout=TIMEOUT,
    )

    print_request('POST', response.url, headers)
    handle_response(response, 'Like mark')

    return response.json()


def get_liked_marks(session, hostname, token):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    base_url = get_url(hostname, 'mark')
    url = "%s/liked" % base_url

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT,
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get liked marks')

    return response.json()


def comment_mark(session, hostname, token, mark_id, comment, is_private=True, shared_with=''):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
    }
    data = json.dumps({
        'description': comment,
        'isPrivate': is_private,
        'sharedWith': shared_with,
    })
    base_url = get_url(hostname, 'mark')
    url = "%s/%s/comment" % (base_url, mark_id)

    response = session.post(
        url,
        headers=headers,
        data=data,
        timeout=TIMEOUT,
    )

    print_request('POST', response.url, headers, data)
    handle_response(response, 'Comment mark')

    return response.json()


def get_mark_comments(session, hostname, token, mark_id):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    base_url = get_url(hostname, 'mark')
    url = "%s/%s/comment" % (base_url, mark_id)

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT,
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get mark comments')

    return response.json()
