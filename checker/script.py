from sys import argv

from methods.health_check import health_check
from methods.put_flag import put_flag
from methods.get_flag import get_flag
from methods.info import info

from methods.result import (
    Result,
    ERROR,
)

if __name__ == '__main__':
    result = None
    if len(argv) > 1:
        if argv[1] == "check":
            if len(argv) > 2:
                result = health_check(argv[2])
        elif argv[1] == "put":
            if len(argv) > 4:
                result = put_flag(argv[2], argv[3], argv[4])
        elif argv[1] == "get":
            if len(argv) > 5:
                result = get_flag(argv[2], argv[3], argv[4], argv[5])
        elif argv[1] == "info":
            result = info()
            print(result.extra_data)
        if result:
            print("\nStatus: %s" % result.status)
            exit(result.status)
    exit(ERROR)
